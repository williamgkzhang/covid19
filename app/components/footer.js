function Contact () {
  return (
    <div className='pr-4'>
      <h3 className="text-blue-700 text-xl font-bold underline">Contact</h3>
    Made by <a className="text-blue-600" href="https://twitter.com/gongonzabar">@gongonzabar</a>
      <br />

    Requests or questions? &nbsp;
      <a className="text-blue-600" href="https://gitlab.com/infinitesecond/covid19/-/issues">File an Issue on Gitlab</a>
    </div>
  )
}

function Attributions () {
  return (
    <div className='pl-4'>
      <h3 className="text-blue-700 text-xl font-bold underline">Data Source</h3>
    Made with data from&nbsp;
      <a
        href="https://usafacts.org/visualizations/coronavirus-covid-19-spread-map/"
        rel="noopener nofollow noreferrer"
        className="underline font-semibold text-purple-600"
      >
        USAFacts.
      </a>

      <br />
    Thank you USAFacts!
    </div>
  )
}

function Footer () {
  return (
    <div className='flex justify-center p-6 mt-4 bg-indigo-200'>
      <Contact />
      <Attributions />
    </div>
  )
}

export default Footer
