import { memo } from 'react'
import { useQuery } from 'react-query'

import { getNewCasesByCounties } from '../data/index'

import LineGraph from './lineGraph'

const customProperties = {
  curve: 'monotoneX'
}

function NewCases ({ selectedCounties }) {
  const { data, status } = useQuery(['new_cases', { ids: selectedCounties }], getNewCasesByCounties)
  const shouldShow = status === 'success' && selectedCounties.length > 0

  return shouldShow && (
    <div className="chart transition duration-500 ease-in-out pt-5 mt-5 rounded overflow-hidden shadow-lg bg-white" style={{ height: '700px' }}>
      <h2 className="text-black text-xl font-bold pb-2 pl-5">New Cases Per Day</h2>
      <LineGraph data={data} customProperties={customProperties} />
    </div>
  )
}

const MemoizedNewCases = memo(NewCases)

export default MemoizedNewCases
