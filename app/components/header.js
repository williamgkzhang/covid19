function Header () {
  return (
    <>
      <div className='flex justify-center py-4 bg-indigo-200 shadow-md'>
        <img className='h-12 w-12 mr-2' alt="covid-picture" src='./virus.svg' />
        <h1 className='text-blue-900 text-4xl font-bold'>G.K.&apos;s COVID-19 Data Visualizer </h1>
      </div>
    </>
  )
}

export default Header
